const {ObjectID} = require('mongodb');

const {mongoose} = require('./../server/db/mongoose');
const {Todo} = require('./../server/models/todo');
const {User} = require('./../server/models/user');

// Todo.remove({}).then((result) => {
//     console.log(result);
// })

// Todo.findOneAndRemove()

// Todo.findOneAndRemove({_id: '5ab1290b81d4b1ca252f0a4c'}).then((todo) => {

// });

Todo.findByIdAndRemove('5ab1290b81d4b1ca252f0a4c').then((todo) => {
    console.log(todo);
});